/**
 * uet.js
 * @Author: lazyphp
 * @link: http://uetjs.ucentu.com
 * @Version: 1.3
 */

/**
 * 本JS骨架有助于改善JS代码混乱的问题
 * 通过简单的配置，可以做到不同页面加
 * 载不同的JS代码片段。犹如PHP框架的
 * 调用形式，更加直观地将相同的公用
 * 和独立的JS代码片段分离和整合。
 * 关于原理：
 * 其实就是利用createElement实现JS的
 * 动态加载。
 */

    /**
     * 加载的JS文件版本号 |缺省值: 当期uet的版本号
     * @type {string}
     */
var	version = "?v=1.2",

    /**
     * 目录地址 | 缺省值: 空
     * @type {string}
     */
    path = '',

    /**
     * js文件后缀
     * @type {string}
     */
    jsSuffix = '.js',

    /**
     * js文件后缀
     * @type {string}
     */
    cssSuffix = '.css',

    uetjs = {};

/**
 * 加载JS文件
 * @param fileData 加载的文件
 */
function loadScript(fileData){
    loadRoute(fileData, 'script');
    document.onreadystatechange = lastLoadScript;
}

/**
 * 只有待页面加载完毕，才执行javascript的片段代码 
 * @returns {undefined}
 */
function lastLoadScript() {
    if (document.readyState == "complete") {
        setTimeout("snippets()", 1);
    }
}

/**
 * 加载样式文件
 * @param fileData 加载文件
 */
function loadStyle(fileData){
    loadRoute(fileData, 'link');
}

/**
 * 加载路由
 * @param fileData
 * @param type
 */
function loadRoute(fileData, type){
    var suffix = '';
    if(type == 'link'){
        suffix = '.css';
    }else{
        suffix = '.js';
    }
    for(key in fileData){
        var loadVersion = fileData[key]['version'] == undefined ? version : fileData[key]['version'];
        var loadPath = fileData[key]['path'] == undefined ? path : fileData[key]['path'];
        loadFile = loadPath+fileData[key]['name']+suffix+loadVersion;
        require(loadFile, type)
    }
}

/**
 * 加载js文件
 * @param {type} url 文件地址
 * @param {type} type 文件类型 | 缺省值: script
 */

function require(url, type) {
    if(type == undefined){
        type = 'script';
    }
    var node = document.createElement(type);

    if(type == 'script'){
        /* 此处必须禁止异步，防止加载关系打乱 */
        node.async = false;
        node.setAttribute('type', 'text/javascript');
        node.setAttribute('src', url);
        document.getElementsByTagName('head')[0].appendChild(node);
        /* IE8及以前的版本必须保留加载的文件，否则会报错 */
        var isIE6= /msie 6/i.test(navigator.userAgent);
        var isIE7= /msie 7/i.test(navigator.userAgent);
        var isIE8= /msie 8/i.test(navigator.userAgent);
        if(!isIE6 && !isIE7 && !isIE8){
            document.getElementsByTagName('head')[0].removeChild(node);
        }
    }else{
        node.rel = 'stylesheet';
        node.href = url;
        document.getElementsByTagName('head')[0].appendChild(node);
    }
}

/**
 * 加载代码片段
 */
function snippets(){
    for(var key in uetjs){
        var node = document.createElement("script");
        node.textContent = uetjs[key]();
        document.getElementsByTagName('head')[0].appendChild(node);
        /* IE8及以前的版本必须保留加载的文件，否则会报错 */
        var isIE6= /msie 6/i.test(navigator.userAgent);
        var isIE7= /msie 7/i.test(navigator.userAgent);
        var isIE8= /msie 8/i.test(navigator.userAgent);
        if(!isIE6 && !isIE7 && !isIE8){
            document.getElementsByTagName('head')[0].removeChild(node);
        }
    }
}