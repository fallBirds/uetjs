uetjs
====================
Uetjs是一款用于统一加载和管理 CSS文件/JS文件的JavaScript库。  
经测试，兼容目前市面上所有浏览器：IE6-IE11，Chrome，Firefox, Opear。且不依赖任何第三方JavaScript库，纯原生态，下载即可马上使用。
如何使用
====================  
请访问：http://uetjs.ucentu.com